<?php require 'navbar.php'; ?>
<hmtl>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="container mt-5">
        <div class="alert alert-dark">
            <H1 class="display-3">Konto erstellen</H1>
            <div class="form">
                <form action="create_account_controller.php" method="POST">
                    <div class="form-group">
                        <label for="InputForeName">Vorname</label></br>
                        <input type="text" class="form-control" id="InputForeName" name="foreName"  placeholder="Vorname"/></br>
                    </div>
                    <div class="form-group">
                        <label for="InputLastName">Nachname</label></br>
                        <input type="text" class="form-control" id="InputLastName" name="sureName"  placeholder="Nachname"/></br>
                    </div>
                    <div class="form-group">
                        <label for="InputBirthDate">Geburtsdatum</label></br>
                        <input type="date" class="form-control" placeholder="2000-08-19" id="InputBirthDate" name="birthDate"/></br>
                    </div>
                    <div class="form-group">
                        <label for="InputEmail">E-Mail-Adresse</label></br>
                        <input type="email" class="form-control" id="InputEmail" name="email" placeholder="E-Mail-Adresse"/></br>
                    </div>
                    <div class="form-group">
                        <label for="InputPassword1">Passwort eingeben</label></br>
                        <input type="password" class="form-control" id="InputPassword1" name="password1" placeholder="Passwort"/></br>
                    </div>
                    <div class="form-group">
                        <label for="InputPassword2">Passwort bestätigen</label></br>
                        <input type="password" class="form-control" id="InputPassword2" name="password2" placeholder="Passwort"/></br>
                    </div>

                    <input type="submit" class="btn btn-success" value="Konto erstellen"/>
                </form>
            </div>
        </div>
    </div>
</body>
</hmtl>
