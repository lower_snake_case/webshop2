<?php

class Database
{

    private $link;
    private static $db;

    public function __construct($host, $username, $passwd, $dbname)
    {

        $this->link = new mysqli($host, $username, $passwd, $dbname);

        if(!$this->link)
        {
            die("Verbindung gescheitert.<br>Fehlernummer".$this->link->connect_errno());
        }

    }
    public function sendQuery($query)
    {

        $res = $this->link->query($query);

        if(!$res)
        {
            echo 'Query war fehlerhaft<br>Die Query war: '.$query;

        }

        if($this->link->error)
        {
            echo $this->link->error;

        }

        return $res;

    }

    public function ecapeString($string)
    {
        return $this->link->escape_string($string);


    }
    
}
