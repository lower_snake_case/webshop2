<?php require 'navbar.php'; ?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="container mt-5">
        <div class="alert alert-dark">
            <h1 class="display-3">Anmelden</h1>
            <form action="login_controller.php" method="POST">
                <div class="form-group">
                    <label for="InputEmail">E-Mail-Adresse</label></br>
                    <input class="form-control" type="email" id="InputEmail" name="email" placeholder="E-Mail"/></br>
                </div>

                <div class="form-group">
                    <label for="InputPassword">Passwort</label></br>
                    <input class="form-control" type="password" id="InputPassword" name="password" placeholder="Passwort"/></br>
                </div>
                <input class="btn btn-success" type="submit" value="Anmelden"/>
            </form>
        </div>
    </div>
</body>
</html>
