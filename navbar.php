<?php
    session_start();
    /// TODO: fix mobile button; fix active efect (line: 22)
?>
<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        body { background: #808080 !important; }
    </style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="container mt-5">
        <nav class="navbar navbar-dark bg-dark navbar-expand-lg" role="navigation">
            <a class="navbar-brand" href="show_article.php">Webshop2</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigationbar" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navigationbar">
              <div class="navbar-nav">
                <a class="nav-item nav-link active" href="show_article.php">Artikel<span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" href="create_account.php">Konto erstellen</a>
                <a class="nav-item nav-link" href="logout.php">Abmelden</a>
                <a class="nav-item nav-link" href="login.php">Anmelden</a>
                <a class="nav-item nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Meine Bestellungen</a>
                <a class="nav-item nav-link" href="show_basket.php">Warenkorb</a>
              </div>
            </div>
        </nav>
    </container>
</body>
</html>
