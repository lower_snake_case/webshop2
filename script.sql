CREATE DATABASE webshop;
USE webshop;

CREATE TABLE customer 
(
    id int auto_increment,
    fore_name varchar (255) NOT NULL,
    sure_name varchar (255) NOT NULL,
    birthdate date,
    password varchar(255) NOT NULL,
    email varchar (255) NOT NULL,

    PRIMARY KEY (id)
);

CREATE TABLE address
(
    id int auto_increment,
    customerFID int NOT NULL,
    plz varchar(5) NOT NULL,
    street varchar(255) NOT NULL,
    house_nummer char(10) NOT NULL,

    PRIMARY KEY (id),
    FOREIGN KEY (customerFID) REFERENCES customer(id) ON UPDATE CASCADE
);

CREATE TABLE category
(
    id int auto_increment,
    label varchar(255) NOT NULL,

    PRIMARY KEY (id)
);

CREATE TABLE article
(
    id int auto_increment,
    categoryFID int NOT NULL,
    cost decimal(10,2) DEFAULT 0.0,
    label varchar(255) NOT NULL,
    discription varchar (255) NOT NULL,

    PRIMARY KEY (id),
    FOREIGN KEY (categoryFID) REFERENCES category(id) ON UPDATE CASCADE
);

CREATE TABLE basket
(
    id int auto_increment,
    articleFID int NOT NULL,
    amount int DEFAULT 1,
    subtotal decimal(10,2) DEFAULT 0.0,

    PRIMARY KEY (id, articleFID),
    FOREIGN KEY (articleFID) REFERENCES article(id) ON UPDATE CASCADE
);

CREATE TABLE purchase
(
    id int auto_increment,
    customerFID int NOT NULL,
    basketFID int NOT NULL,
    total_cost decimal(10,2) DEFAULT 0.0,
    delivery_date date,

    PRIMARY KEY (id),
    FOREIGN KEY (customerFID) REFERENCES customer(id) ON UPDATE CASCADE,
    FOREIGN KEY (basketFID) REFERENCES basket(id) ON UPDATE CASCADE
);
