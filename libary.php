<?php

session_start();

require 'db.php';

$_POST['db'] = new Database('localhost', 'omarg_jan', 'asdfg', 'omarg_webshop');

function CreateCustomer ($theForeName,
                         $theSureName,
                         $theBirthDate,
                         $theEmail,
                         $thePassword1,
                         $thePassword2)
{

    if ($thePassword1 == $thePassword2) {

        /// hashing password
        $thePassword1 = md5($thePassword1);

        /// creating query
        $query = 'INSERT INTO customer' .
        '(fore_name, sure_name, birthdate, email, password) VALUES ' .
        '("' . $theForeName . '", "' . $theSureName . '", '.
        '"' . $theBirthDate . '", "' . $theEmail . '", "'. $thePassword1 . '")';

        /// sending query
        $_POST['db']->sendQuery($query);

        $_POST['error'] = 0;

        header("Location: login.php");
        exit;
    }
    else {
      $_POST['error'] = 1;
      header("Location: create_account.php");
      exit;
    }

}

function LogIn ($theEmail, $thePassword)
{

    /// Selecting user data
    $query  = 'SELECT id, password FROM customer WHERE email = "' . $theEmail . '"';
    $result = $_POST['db']->sendQuery($query);
    /// User needs to be in the first (and only) tupel
    $row    = mysqli_fetch_row($result);

    /// when password is matching
    if ($row[1] === md5($thePassword)) {

        if (session_start()) {
            /// save user in session
            $_SESSION['user']   = array("id" => $row[0], "mail" => $theEmail);
            /// create empty basket for user
            $_SESSION['basket'] = array();
        }

        /// Segue to articles
        header("Location: show_article.php");
        exit;

    }
    else {
        $_POST['error']="Ihre E-Mail oder Passwort ist falsch\nBitte versuchen Sie es erneut";
        header("Location: login.php");
        exit;

    }

}


function addArticleToBasket($theArticle = array ())
{
    /// Article exists
    if (!($theArticle === array () || $theArticle === NULL)) {
        array_push($_SESSION['basket'], $theArticle);

    }

}

/* TODO: fix redundant db call
 *       create keys for array
 *       add amount to array
 **/
function createArticleList()
{

     $query = 'SELECT article.id AS id, article.label AS Produkt,
     article.discription AS Beschreibung, category.label AS Kategorie,
     article.cost AS Euro FROM article
     JOIN category ON article.categoryFID = category.id';

     $result = $_POST['db']->sendQuery($query);

     /// Create article list in POST
     $_POST['article_group'] = array ();

     /// For every article in our result
     while ($article = mysqli_fetch_row($result)) {

         /// Push back the current article
         array_push ($_POST['article_group'],
             array (
                 'id'          => $article[0],
                 'label'       => $article[1],
                 'description' => $article[2],
                 'category'    => $article[3],
                 'unit_price'  => $article[4],
             )
         );

     }

}

/** TODO: center columns
  *       add amount
  *       increment double added items (map?)
  */
function showArticle()
{
    /// Default for empty index


    $query = 'SELECT article.id AS id, article.label AS Produkt, article.discription AS Beschreibung, category.label AS Kategorie, article.cost AS Euro FROM article JOIN category ON article.categoryFID = category.id';

    $result = $_POST['db']->sendQuery($query);

    createArticleList();

    echo '<form method="POST" action="#">' . PHP_EOL;
    echo '<div class="table-responsive-sm">' . PHP_EOL;
    echo "<table class='table table-dark table-striped'>\n";
    echo "<thead><tr><th scope='col'>ID</th><th scope='col'>Artikel</th><th scope='col'>Beschreibung</th><th scope='col'>Kategorie</th><th scope='col'>Preis</th><th scope='col'>Hinzufügen</th></tr></thead>";

    echo "<tbody>";
    $index = 0;
    while ($row = mysqli_fetch_row($result)) {
        echo "\t<tr>\n";
        echo "\t<th scope='row'>" . $row[0] . "</th>" . PHP_EOL;

        for ($i = 1; $i < sizeof($row); $i++) {
            echo "\t\t<td>{$row[$i]}</td>\n";

        }
        echo "\t\t" . '<td class="radio"><input type="radio" name="article" value="' . $index . '"/></td>' . PHP_EOL;
        echo "\t</tr>\n";

        $index++;

    }
    echo "</tbody>\n";
    echo "<tr><td></td><td></td><td></td><td></td><td></td><td><input class='mt-2 btn btn-success' type='submit' value='In den Warenkorb'/></td></tr>" . PHP_EOL;
    echo "</table>\n";
    echo "</div>\n";
    echo "</form>\n";

    if (isset($_SESSION['basket']) && $_POST['article'] !== NULL) addArticleToBasket($_POST['article_group'] [$_POST['article']]);

}

function printBasket()
{

    echo '<div class="table-responsive-sm mt-5">' . PHP_EOL;
    echo "<table class='table table-dark table-striped'>\n";
    echo "<thead><tr><th scope='col'>ID</th><th scope='col'>Artikel</th><th scope='col'>Beschreibung</th><th scope='col'>Kategorie</th><th scope='col'>Preis</th></tr></thead>";

    echo "<tbody>";

    foreach ($_SESSION['basket'] as $article) {
      
      echo "\t<tr>\n";
      echo "\t<th scope='row'>" . $article['id'] . "</th>" . PHP_EOL;

      echo "\t\t<td>{$article ["label"]}</td>\n";
      echo "\t\t<td>{$article ["description"]}</td>\n";
      echo "\t\t<td>{$article ["category"]}</td>\n";
      echo "\t\t<td>{$article ["unit_price"]}</td>\n";

      echo "\t</tr>\n";

    }
    echo "</tbody>\n";
    echo "</table>\n";
    echo "</div>\n";

}

function showBasket()
{
    if (!isset($_SESSION['user'])) {

      echo '<div class="mt-5 alert alert-danger" role="alert"><a class="lead text-danger" href="login.php">Bitte melden Sie sich an!</a></div>';

    }
    else {
        printBasket($_SESSION['id']); // TODO: add function

    }
}

function logout ()
{

    session_destroy();

    header("Location: login.php");
    exit;

}
